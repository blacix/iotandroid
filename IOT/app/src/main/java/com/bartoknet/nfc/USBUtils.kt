package com.bartoknet.nfc

import android.content.Context
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.util.Log

class USBUtils {

    companion object {
        fun findDevice(context: Context, vid: Int, pid: Int): UsbDevice? {
            val manager = context.getSystemService(Context.USB_SERVICE) as UsbManager

            val devices = manager.deviceList
            devices.entries.forEach { entry ->
                Log.d(TAG, "findDevice - device: " + entry.key + " " + entry.value.productName
                        + " vid " + "%02x".format(entry.value.vendorId) + " pid " + "%02x".format(entry.value.productId))

                if(entry.value.vendorId == vid && entry.value.productId == pid) {
                    return entry.value
                    Log.d(TAG, "device found")
                }
            }
            return null
        }
    }
}