package com.bartoknet.nfc

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbConstants
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.util.Log
import com.hoho.android.usbserial.driver.*
import java.lang.IllegalArgumentException
import java.util.concurrent.atomic.AtomicBoolean


private const val ACTION_USB_ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED"
private const val ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION"


class STMIOTADevice(context: Context) {


//    private val TAG = STMIOTADevice::class.simpleName // runtime

    // static compile time == static
    companion object {
        const val VID: Int = 0x0483 // compile time
        const val PID: Int = 0x374b // compile time
    }

    private val mContext: Context = context
    private var mUsbManager: UsbManager
    private var mUsbDevice: UsbDevice?
    private var mUsbSerialPort: UsbSerialPort? = null
    private var mBuffer: ByteArray = ByteArray(1024)
    private var mDataIn: ByteArray = ByteArray(1024)
    private lateinit var mWorkerThread: Thread
    private var mRunning: AtomicBoolean = AtomicBoolean(false)


    fun requestPermission() {
        val filter = IntentFilter(ACTION_USB_PERMISSION)
        mContext.registerReceiver(mUsbPermissionReceiver, filter)

        val permissionIntent = PendingIntent.getBroadcast(mContext, 0, Intent(ACTION_USB_PERMISSION), 0)
        if(mUsbDevice != null) {
            mUsbManager.requestPermission(mUsbDevice, permissionIntent)
        } else {
            Log.e(TAG, "requestPermission - no USB device")
        }
    }

    fun disconnect() {
        try {
            mContext.unregisterReceiver(mUsbPermissionReceiver)
        } catch (exception: IllegalArgumentException) {

        }
        stopReading()
    }

    fun startReading() {

        mUsbSerialPort = findPort()
        if (mUsbSerialPort != null) {
            mRunning.set(true)

            mWorkerThread = object: Thread() {
                override fun run() {
                    mUsbSerialPort?.let { readLoop(it) }
                }
            }

            mWorkerThread.start()

        }
    }


    private fun readLoop(usbSerialPort: UsbSerialPort) {
        var message: String = ""
        while (mRunning.get()) {
            var len = usbSerialPort.read(mBuffer, 2000);
            if(len > 0) {
                var dataIn = mBuffer.slice(IntRange(0, len-1)).toByteArray().toString(Charsets.US_ASCII)
                // Log.d(TAG, "readLoop - dataIn: $dataIn")
                var cnt = 0
                while(cnt < dataIn.length) {
                    val currentChar = dataIn[cnt]
                    if(currentChar != '\n') {
                        message += currentChar
                    } else {
                        Log.d(TAG, "readLoop - message $message")
                        message = ""
                    }

                    cnt = cnt.inc()
                }

            }
        }
    }

    fun stopReading() {
        mRunning.set(false);
        mWorkerThread.join()
    }

    private fun findPort(): UsbSerialPort? {
        var customTable = ProbeTable()
        customTable.addProduct(VID, PID, CdcAcmSerialDriver::class.java)

        var prober = UsbSerialProber(customTable);
        var availableDrivers: Array<UsbSerialDriver> = prober.findAllDrivers(mUsbManager).toTypedArray()
        Log.d(TAG, "findPort - available drivers: " + availableDrivers.size)
        if(availableDrivers.isNotEmpty()) {
            var driver = availableDrivers[0];
            val connection = mUsbManager.openDevice(driver.device);
            if(connection != null) {
                var port = driver.ports[0]
                port.open(connection)
                port.setParameters(115200, UsbSerialPort.DATABITS_8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE)
                return port
            }
        }

        return null
    }


    private val mUsbPermissionReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (ACTION_USB_PERMISSION == intent.action) {
                synchronized(this) {
                    val device: UsbDevice? = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE)

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        Log.d(TAG, "permission granted $device")

                    } else {
                        Log.d(TAG, "permission denied for device $device")
                    }
                }
            }
        }
    }

    private val mBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d(TAG, "USB broadcast")
            when(intent?.action) {
                ACTION_USB_ATTACHED -> {

                }
            }
        }
    }





    // native API

    fun workNative() {
        var mUsbThread: Thread = object : Thread() {
            override fun run() {
                mBuffer[0] = 'h'.toByte()
                mBuffer[1] = 'e'.toByte()
                mBuffer[2] = 'l'.toByte()
                mBuffer[3] = 'l'.toByte()
                mBuffer[4] = 'o'.toByte()


                Log.d(TAG, "ifconunt " + mUsbDevice?.interfaceCount)
                for (i in 0 until (mUsbDevice?.interfaceCount ?: 0)) {
                    mUsbDevice?.getInterface(i).also {
                        Log.d(TAG, "if " + it.toString() + " endpoints: " + it?.endpointCount)
                    }.also {
                        for (j in 0 until (it?.endpointCount ?: 0)) {
                            if (it?.getEndpoint(j)?.type == UsbConstants.USB_ENDPOINT_XFER_BULK && it?.getEndpoint(
                                    j
                                )?.direction == UsbConstants.USB_DIR_IN
                            ) {
                                Log.d(TAG, "endpoint found: " + it?.getEndpoint(j)?.toString())


                                mUsbManager.openDevice(mUsbDevice).apply {
                                    // controlTransfer()
                                    claimInterface(it, true)
                                    bulkTransfer(it?.getEndpoint(j), mBuffer, 10, 1000)
                                    Log.d(TAG, "USB read: " + mBuffer.toString(Charsets.UTF_8))
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    init {
        mUsbManager = mContext.getSystemService(Context.USB_SERVICE) as UsbManager
        mUsbDevice = USBUtils.findDevice(mContext, VID, PID)
    }
}