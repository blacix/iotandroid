package com.bartoknet.nfc

import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


private val TAG = MainActivity::class.simpleName // runtime

class MainActivity : AppCompatActivity() {

    lateinit var mStmUsbDevice: STMIOTADevice

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mStmUsbDevice = STMIOTADevice(this)

        this.findViewById<Button>(R.id.button_permission).setOnClickListener {
            mStmUsbDevice.requestPermission()
        }

        this.findViewById<Button>(R.id.button_start).setOnClickListener {
            mStmUsbDevice.startReading()
            EventBus.getDefault().post(MessageEvent())
        }

        this.findViewById<Button>(R.id.button_stop).setOnClickListener {
            mStmUsbDevice.stopReading()
        }
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this);
    }

    override fun onPause() {
        super.onPause()
        mStmUsbDevice.disconnect()
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent?) {
        if (event != null) {
            Log.d(TAG, "onMessageEvent " + event.mMessage)
        }
    }

    class MessageEvent {
        var mMessage = "hello"

    }

}
