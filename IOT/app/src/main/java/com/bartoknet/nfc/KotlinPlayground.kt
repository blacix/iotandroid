package com.bartoknet.nfc

class KotlinPlayground {

    companion object {
        val SOME_CONST = 0 // compile time
        const val VID: Int = 0x0483 // compile time
        const val PID: Int = 0x374b // compile time
    }

    object Stuff {
        const val VID: Int = 0x0000
    }



    private var mInitWithLambda = { param: Int ->
        if(param == 0)
            1
        else
            11
    }(0)

}